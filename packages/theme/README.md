## This repo is under development

# @universis/theme
[Universis](https://universis.gr) is a coordinated effort by Greek academic institutions to build a Student Information System as an open source platform. The target is to serve our common needs to support academic and administrative processes.

**@universis/theme** package contains the core theme for building client applications or components for Universis project
