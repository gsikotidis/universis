import { Component, AfterViewInit, ElementRef, Renderer2, OnInit } from '@angular/core';

/**
 *
 * Uses ng-spin-kit to show a spinner
 * @export
 * @class SpinnerComponent
 * @implements {OnInit}
 */
// @ts-ignore
@Component({
  selector: 'universis-spinner',
  template: `
  <div [ngStyle]="{'text-align': 'center',
  'display': 'block'}">
  <sk-three-bounce></sk-three-bounce>
  </div>`
})
export class SpinnerComponent implements OnInit {

  constructor(private el: ElementRef, private renderer: Renderer2) { }

  ngOnInit() {
    this.renderer.setStyle(this.el.nativeElement.parentElement, 'background-color', 'transparent');
    this.renderer.setStyle(this.el.nativeElement.parentElement, 'border', '0px');
  }
}
